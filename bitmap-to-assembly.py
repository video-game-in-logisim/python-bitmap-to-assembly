#!/usr/bin/env python

import sys
from PIL import Image

img = Image.open(sys.argv[1])  # open the image given in the command

flattenedImg = img.getdata()

binary = []

for pixel in flattenedImg:
    if pixel[0] == 0:
        binary.append(0)
    else:
        binary.append(1)

memory_address = int(511-127)

# every 8 bits, print a move command to the next y address
for i in range(int(len(binary)/8)):
    binary_portion = ''.join(map(str, binary[i*8:i*8+8]))

    print('MOV [' + str(memory_address) + '], ' + binary_portion + 'b')
    memory_address += 1

print('HLT')